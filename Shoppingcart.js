//Author : Karthikeyan Shanmugam
//Student ID : 8740880

// importing dependencies 
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

const { check, validationResult } = require('express-validator');

// setting up variables to use packages
var myApp = express();
myApp.use(bodyParser.urlencoded({extended:false}));

// set up DB connection
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/gamingCart', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

// set up the model for the order in database
const Order = mongoose.model('Order',{
    name : String,
    email : String,
    phone : Number, 
    address : String, 
    city : String,
    province : String,
    postcode : String,
    delivery : String,
    playstation : Number,
    xbox : Number,
    playstationPrice : Number,
    xboxPrice : Number,
    subTotal : Number,
    tax : Number,
    shipping : Number,
    total : Number
});

// set path to public folders and view folders
myApp.set('views', path.join(__dirname, 'views'));

//use public folder for CSS etc.
myApp.use(express.static(__dirname+'/public'));
myApp.set('view engine', 'ejs');


//postive number regex validation
var positiveNum = /^[1-9][0-9]*$/;

//phone regex validation
var phoneRegex = /^[0-9]{10}$/;

//email regex validation
//var emailRegex = /^[a-z]+@[a-z]+(?:\.[a-z]+)*$/;
var emailRegex = /^[a-z]{4}[\@][a-z]{4}[\.][a-z]{3}$/;

//postcode regex validation
var postcodeRegex = /^[A-Z][0-9][A-Z][\s][0-9][A-Z][0-9]$/;

// function to check regex validation
function checkRegex(userInput, regex){
    if(regex.test(userInput)){
        return true;
    }
    else{
        return false;
    }
}

//custom phone validation
function customPhoneValidation(value){
    if(!checkRegex(value, phoneRegex)){
        throw new Error('Phone should be in format XXXXXXXXXX');
    }
    return true;
}

//custom email validation
function customEmailValidation(value){
    if(!checkRegex(value, emailRegex)){
        throw new Error('Email should be in format test@test.com');
    }
    return true;
}

//custom postcode validation
function customPostCodeValidation(value){
    if(!checkRegex(value, postcodeRegex)){
        throw new Error('Postcode should be in format eg: X9X 9X9');
    }
    return true;
}

//Total cost validation
function customTotalcostValidations(value, {req}){
    var playstationQty = req.body.playstation;
    var xboxQty = req.body.xbox;
    if((!checkRegex(playstationQty, positiveNum)) && (!checkRegex(xboxQty, positiveNum))){
        throw new Error('Please enter a valid number in product quantity');
    }
    else if(Number((playstationQty*4)+(xboxQty*5)) < 10){
        throw new Error('Please select more quantity, Minimum purchase cost is 10$');
    }
    return true;
}

//Setting path to the homepage
myApp.get('/', function(req, res){
    res.render('form');
});

//Error validations for all the fields
myApp.post('/',[
    check('name', 'Enter a valid name').notEmpty(),
    check('email').custom(customEmailValidation),
    check('phone').custom(customPhoneValidation),
    check('address','Address is required').notEmpty(),
    check('city','City is required').notEmpty(),
    check('province','Select the province').notEmpty(),
    check('postcode').custom(customPostCodeValidation),
    check('delivery','Select the delivery time').notEmpty(),
    check('playstation','xbox').custom(customTotalcostValidations)
], function(req, res){
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        res.render('form', {
            errors : errors.array()
        })
    }
    else{
        //Getting all the input values from fields
        var name = req.body.name;
        var email = req.body.email;
        var phone = req.body.phone;
        var address = req.body.address;
        var city = req.body.city;
        var province = req.body.province;
        var postcode = req.body.postcode
        var delivery = req.body.delivery;
        var playstation = req.body.playstation;
        var xbox = req.body.xbox;

        //Calculating price and total cost
        var playstationPrice = playstation * 4;
        var xboxPrice = xbox * 5;
        var subTotal = playstationPrice + xboxPrice
        
        //Validation tax based on the selected province
        switch(province)
        {
            case 'Alberta':
                var tax = subTotal * 0.05;
                break;
            case 'British Columbia':
                var tax = subTotal * 0.12;
                break;
            case 'Manitoba':
                var tax = subTotal * 0.12;
                break;
            case 'New Brunswick':
                var tax = subTotal * 0.15;
                break;   
            case 'Newfoundland and Labrador':
                var tax = subTotal * 0.15;
                break;    
            case 'Nova Scotia':
                var tax = subTotal * 0.15;
                break;  
            case 'Ontario':
                var tax = subTotal * 0.13;
                break;  
            case 'Prince Edward Island':
                var tax = subTotal * 0.15;
                break;  
            case 'Quebec':
                var tax = subTotal * 0.11;
                break; 
            case 'Saskatchewan':
                var tax = subTotal * 0.05;
                break; 
            case 'Northwest Territories':
                var tax = subTotal * 0.12;
                break; 
            case 'Nunavut':
                var tax = subTotal * 0.12;
                break; 
            case 'Yukon':
                var tax = subTotal * 0.12;
                break; 
        }
        var shipping = 20;
        var total = subTotal + tax + shipping;

        //objects to print all data
        var pageData = {
            name : name,
            email : email,
            phone : phone, 
            address : address, 
            city : city,
            province : province,
            postcode : postcode,
            delivery : delivery,
            playstation : playstation,
            xbox : xbox,
            playstationPrice : playstationPrice,
            xboxPrice : xboxPrice,
            subTotal : subTotal,
            tax : tax,
            shipping : shipping,
            total : total
        }
        
        // store the order to the database
        var newOrder = new Order(pageData);
        // save order
        newOrder.save().then(function(){
            console.log('New order created');
        });
        res.render('form', pageData);
    }    
});

// All orders page
myApp.get('/allorders', function(req, res){
    Order.find({}).exec(function(err, orders){
        console.log(err);
        res.render('allorders', {orders: orders});
    });
});

// start the server and listen at a port
myApp.listen(8080);

//Verfication for code execution
console.log('Program executed and website port 8080 is started');